#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "syntree.h"

node* newNode(int val) {
  node* nn = calloc(sizeof(node),1);
  if(!nn) {
    return NULL;
  }
  nn->val=val;
  return nn;
}

int syntreeInit(Syntree *self) {
  self->root.node = NULL;
  return 0;
}

void _release(node *n) {
  if(n->right_sibling!=NULL) {
    _release(n->right_sibling);
  }
  if(n->leftmost_child!=NULL) {
    _release(n->leftmost_child);
  }
  free(n);
}
void syntreeRelease(Syntree *self) {
  _release(self->root.node);
}

SyntreeNodeID syntreeNodeNumber(Syntree *self, int number) {
  SyntreeNodeID id;
  node* n = newNode(number);
  if(!n) {
    fprintf(stderr,"Error in syntreeNodeNumber()\n");
    exit(EXIT_FAILURE);
  }
  id.node = n;
  return id;
}

SyntreeNodeID syntreeNodeTag(Syntree *self, SyntreeNodeID id) {
  SyntreeNodeID parent = syntreeNodeNumber(self,-1);
  parent.node->leftmost_child = id.node;
  id.node->parent = parent.node;
  if(self->root.node == NULL) {
    self->root.node = parent.node;
  }
  self->root = parent;
  return parent;
}

SyntreeNodeID
syntreeNodePair(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2) {
  SyntreeNodeID parent = syntreeNodeNumber(self,-1);
  parent.node->leftmost_child = id1.node;
  parent.node->leftmost_child->right_sibling = id2.node;
  id1.node->parent = parent.node;
  id2.node->parent = parent.node;
  return parent;
}

SyntreeNodeID
syntreeNodeAppend(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem) {
  node* it = list.node->leftmost_child;
  while(it->right_sibling != NULL)
    it = it->right_sibling;

  elem.node->parent = list.node;
  it->right_sibling = elem.node;
  return list;
}

SyntreeNodeID
syntreeNodePrepend(Syntree *self, SyntreeNodeID elem, SyntreeNodeID list) {
  elem.node->parent = list.node;
  elem.node->right_sibling = list.node->leftmost_child;
  list.node->leftmost_child = elem.node;
  return list;
}

void _print(node* node, int gen) {
  if(node->val != -1) {
    for(int i=0; i<gen; ++i)
      printf("\t");
    printf("(%d)\n",node->val);
    if(node->right_sibling != NULL && gen > 0)
      _print(node->right_sibling,gen);
    return;
  }

  for(int i=0; i<gen; ++i)
    printf("\t");
  printf("{\n");

  if(node->leftmost_child != NULL) {
    _print(node->leftmost_child,gen+1);
    for(int i=0; i<gen; ++i)
      printf("\t");
    printf("}\n");
  }

  if(node->right_sibling != NULL && gen > 0)
    _print(node->right_sibling,gen);
}
void syntreePrint(const Syntree *self, SyntreeNodeID root) {
  _print(root.node,0);
}
