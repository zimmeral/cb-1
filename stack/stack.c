#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

const size_t INIT_CAP = 10;

int stackInit(IntStack *self) {

  int* buf = malloc(sizeof(int) * INIT_CAP);
  if(!buf) {
    fprintf(stderr, "mem alloc error in stackInit()");
    return EXIT_FAILURE;
  }
  self->buf = buf;
  self->top = 0;
  self->cap = INIT_CAP;
  return 0;
}

void stackRelease(IntStack *self) {
  free(self->buf);
}

int stackTop(const IntStack *self) {
  if(stackIsEmpty(self)) {
    printf("stack{%p} is empty, returning -1\n",(void*)self);
    return -1;
  }
  return self->buf[self->top];
}

int stackIsEmpty(const IntStack *self) {
  return self->top==0;
}

int stackPop(IntStack *self) {
  if(stackIsEmpty(self)) {
    printf("stack{%p} is empty, returning -1\n",(void*)self);
    return -1;
  }
  return self->buf[self->top--];
}

void stackPush(IntStack *self, int i) {
  if(self->top == self->cap-1) {
    self->buf = realloc(self->buf,self->cap+INIT_CAP);
    if(!(self->buf)) {
      fprintf(stderr, "mem alloc error in stackPush()");
      exit(EXIT_FAILURE);
    }
    self->cap += INIT_CAP;
  }
  self->buf[++(self->top)] = i;
}
